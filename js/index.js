'use strict'

//Services
const servicesList = document.getElementsByClassName('services-list-item');
const servicesTextList = document.getElementsByClassName('services-text-list-item');
for (const element of servicesList) {
    element.addEventListener('click', () => {
        for (const servicesListElement of servicesList) {
            servicesListElement.classList.remove('active');
        }
        for (const servicesTextListElement of servicesTextList) {
            servicesTextListElement.classList.remove('active');
            if (servicesTextListElement.classList.contains(element.id)) {
                servicesTextListElement.classList.add('active');
            }
        }
        element.classList.add('active');
    })
}

//Work
const filterList = document.getElementsByClassName('work-list-item');
const photoList = document.getElementsByClassName('work-gallery-item');
/*filter*/
for (const element of filterList) {
    element.addEventListener('click', () => {
        for (const filterListElement of filterList) {
            filterListElement.classList.remove('active');
        }
        let counter = 0
        for (const photoListElement of photoList) {
            if (photoListElement.classList.contains(element.id) || element.id === 'all') {
                if (counter < 12) {
                    photoListElement.classList.remove('hide');
                } else {
                    photoListElement.classList.add('hide');
                }
                counter++;
            } else {
                photoListElement.classList.add('hide');
            }
        }
        if (counter <= 12) {
            document.getElementsByClassName('work-load-more')[0].classList.add('hide');
        } else {
            document.getElementsByClassName('work-load-more')[0].classList.remove('hide');
        }
        counter = 0;
        element.classList.add('active');
    })
}
/*Loader*/
const loader = (loader) => {
    loader.classList.remove('hide');
    setTimeout(() => {
        loader.classList.add('hide');
    }, 1500);
}
/*Load more*/
document.getElementsByClassName('work-load-more')[0].addEventListener('click', function () {
    loader(document.getElementsByClassName('loader')[0]);
    setTimeout(() => {
        let counter = 0
        let containMore = false;
        const active = document.getElementsByClassName('work-list')[0].getElementsByClassName('active')[0].id;
        for (const photoListElement of photoList) {
            if (counter > 11) {
                console.log(photoListElement)
                if (photoListElement.classList.contains('hide')) {
                    containMore = true;
                } else {
                    containMore = false;
                }
                console.log(containMore)
                break;
            }
            if (photoListElement.classList.contains('hide') && (active === 'all' || photoListElement.classList.contains(active))) {
                photoListElement.classList.remove('hide');
                counter++;
            }
        }
        if (!containMore) {
            this.classList.add('hide');
        }
    }, 1500);
})

//Feedback
const commentatorsList = document.getElementsByClassName('commentator');
const commentList = document.getElementsByClassName('comments-item');
const findActive = (list) => {
    for (let i = 0; i < list.length; i++) {
        if (list[i].classList.contains('active')) {
            return i;
        }
    }
}
/*Next commentator*/
document.getElementsByClassName('next-commentator')[0].addEventListener('click', () => {
    let active = findActive(commentatorsList);
    commentatorsList[active].classList.remove('active');
    commentList[active].classList.add('hide');
    active++;
    if (active === commentatorsList.length) {
        active = 0;
    }
    commentatorsList[active].classList.add('active');
    commentList[active].classList.remove('hide');
})
/*Previous commentator*/
document.getElementsByClassName('previous-commentator')[0].addEventListener('click', () => {
    let active = findActive(commentatorsList);
    for (let i = 0; i < commentatorsList.length; i++) {
        if (commentatorsList[i].classList.contains('active')) {
            active = i;
            break;
        }
    }
    commentatorsList[active].classList.remove('active');
    commentList[active].classList.add('hide');
    active--;
    if (active === -1) {
        active = commentatorsList.length - 1;
    }
    commentatorsList[active].classList.add('active');
    commentList[active].classList.remove('hide');
})
/*Choose commentator*/
for (const element of commentatorsList) {
    element.addEventListener('click', () => {
        for (const commentatorsListElement of commentatorsList) {
            commentatorsListElement.classList.remove('active');
        }
        for (const commentListElement of commentList) {
            commentListElement.classList.add('hide');
        }
        element.classList.add('active');
        document.getElementsByClassName('comments')[0].getElementsByClassName(element.id)[0].classList.remove('hide');
    })
}
//Gallery

document.getElementsByClassName('gallery-load-more')[0].addEventListener('click', function () {
    loader(document.getElementsByClassName('loader')[1]);
    setTimeout(() => {
        let counter = 0
        let containMore = false;
        // const active = document.getElementsByClassName('work-list')[0].getElementsByClassName('active')[0].id;
        for (const photoElement of document.getElementsByClassName('gallery-list-item')) {
            console.log(photoElement)
            if (counter > 8) {
                if (photoElement.classList.contains('hide')) {
                    containMore = true;
                } else {
                    containMore = false;
                }
                console.log(containMore)
                break;
            }
            if (photoElement.classList.contains('hide')) {
                photoElement.classList.remove('hide');
                counter++;
            }
        }
        if (!containMore) {
            this.classList.add('hide');
        }
        const msnry1 = new Masonry('.gallery-list', {
            itemSelector: ".gallery-list-item",
            columnWidth: 373,
            gutter: 20,
            horizontalOrder: false
        });
    }, 1500);
})